﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyelvvizsga
{
    class Program
    {
        static Dictionary<string, Dictionary<int, int>> sikeres;
        static Dictionary<string, Dictionary<int, int>> sikertelen;
        static int vizsgalandoEv;

        static void Main(string[] args)
        {
            FillDictionariesFromCSV();

            MasodikFeladat();

            HarmadikFeladat();

            NegyedikFeladat();

            OtodikFeladat();

            HatodikFeladat();

            Console.WriteLine();
            Console.WriteLine("Nyomjon meg egy gombot a befejezéshez...");
            Console.ReadKey();
        }

        private static void HatodikFeladat()
        {
            var temporaryDict = new Dictionary<string, int>();
            var failedExams = new Dictionary<string, int>();
            var failinGrade = new Dictionary<string, double>();

            var resultNumber = 0;

            foreach (var item in sikeres)
            {
                foreach (var yearResult in item.Value)
                {
                    resultNumber += yearResult.Value;
                }
                temporaryDict.Add(item.Key, resultNumber);
                resultNumber = 0;
            }

            resultNumber = 0;

            foreach (var item in sikertelen)
            {

                foreach (var yearResult in item.Value)
                {
                    resultNumber += yearResult.Value;
                }
                temporaryDict[item.Key] += resultNumber;
                failedExams.Add(item.Key, resultNumber);

                resultNumber = 0;

                var all = (double)temporaryDict[item.Key];
                var failed = (double)failedExams[item.Key];

                failinGrade.Add(item.Key, Math.Round(failed / all, 2) * 100);
            }


            using (var f = new StreamWriter(@"CSV files\Output\osszesites.csv", false, Encoding.Default))
            {
                foreach (var item in temporaryDict)
                {
                    var lineToWrite = $"{item.Key};{item.Value};{failinGrade.FirstOrDefault(fr => fr.Key.Equals(item.Key)).Value}%";
                    f.WriteLine(lineToWrite);
                }
            }
        }

        private static void OtodikFeladat()
        {
            var temporaryDict = new Dictionary<string, int>();

            var resultNumber = 0;

            foreach (var item in sikeres)
            {
                foreach (var yearResult in item.Value)
                {
                    if (yearResult.Key == vizsgalandoEv)
                    {
                        resultNumber += yearResult.Value;
                    }
                }
                temporaryDict.Add(item.Key, resultNumber);

                resultNumber = 0;
            }

            resultNumber = 0;

            foreach (var item in sikertelen)
            {
                foreach (var yearResult in item.Value)
                {
                    if (yearResult.Key == vizsgalandoEv)
                    {
                        resultNumber += yearResult.Value;
                    }
                }
                temporaryDict[item.Key] += resultNumber;
                resultNumber = 0;
            }


            bool hadNoExaminee = true;

            Console.WriteLine("5. Feladat:");

            foreach (var item in temporaryDict)
            {
                if (item.Value == 0)
                {
                    hadNoExaminee = false;
                    Console.WriteLine(item.Key);
                }
            }

            if (hadNoExaminee)
            {
                Console.WriteLine("Minden nyelvből volt vizsgázó");
            }
        }

        private static void NegyedikFeladat()
        {
            Console.WriteLine("4. Feladat:");

            var temporaryDict = new Dictionary<string, int>();
            var failedExams = new Dictionary<string, int>();
            var failingrate = new Dictionary<string, double>();

            var resultNumber = 0;

            foreach (var item in sikeres)
            {
                foreach (var yearResult in item.Value)
                {
                    if (yearResult.Key == vizsgalandoEv)
                    {
                        resultNumber += yearResult.Value;
                    }
                }
                temporaryDict.Add(item.Key, resultNumber);
                resultNumber = 0;
            }

            resultNumber = 0;

            foreach (var item in sikertelen)
            {

                foreach (var yearResult in item.Value)
                {
                    if (yearResult.Key == vizsgalandoEv)
                    {
                        resultNumber += yearResult.Value;
                    }
                }
                temporaryDict[item.Key] += resultNumber;
                failedExams.Add(item.Key, resultNumber);

                resultNumber = 0;

                var all = (double)temporaryDict[item.Key];
                var failed = (double)failedExams[item.Key];

                failingrate.Add(item.Key, Math.Round(failed / all, 2) * 100);
            }

            var selectedValue = failingrate.Values.Max();

            Console.WriteLine($"{vizsgalandoEv}-ben {failingrate.FirstOrDefault(fr => fr.Value.Equals(selectedValue)).Key} a(z) sikertelen vizsgák aránya: {selectedValue}%.");
        }

        private static void HarmadikFeladat()
        {
            Console.WriteLine("3. Feladat:");

            Console.WriteLine("Adjon meg egy számot 2009 és 2017 között.");

            var megfelelo = false;

            while (!megfelelo)
            {
                vizsgalandoEv = int.Parse(Console.ReadLine());

                if (vizsgalandoEv > 2009 && vizsgalandoEv < 2017)
                {
                    Console.Write("\t Vizsgálandó év: ");
                    Console.WriteLine(vizsgalandoEv);
                    megfelelo = true;
                }
                else
                {
                    Console.WriteLine("Nem megfelelő évet adott meg, adja meg újra.");
                }
            }
        }

        private static void MasodikFeladat()
        {
            var temporaryDict = new Dictionary<string, int>();

            var resultNumber = 0;

            foreach (var item in sikeres)
            {
                foreach (var yearResult in item.Value)
                {
                    resultNumber += yearResult.Value;
                }

                temporaryDict.Add(item.Key, resultNumber);
                resultNumber = 0;
            }

            resultNumber = 0;

            foreach (var item in sikertelen)
            {
                foreach (var yearResult in item.Value)
                {
                    resultNumber += yearResult.Value;
                }

                temporaryDict[item.Key] += resultNumber;
                resultNumber = 0;
            }

            Console.WriteLine("2. feladat: A legnépszerűbb nyelvek:");

            var counter = 0;

            foreach (var item in temporaryDict.OrderByDescending(d => d.Value))
            {
                Console.WriteLine($"\t{item.Key}");
                counter++;
                if (counter == 3) return;
            }
        }

        public static void FillDictionariesFromCSV()
        {
            sikeres = CSVReader.ReadCSV(@"CSV Files\sikeres.csv");
            sikertelen = CSVReader.ReadCSV(@"CSV Files\sikertelen.csv");
        }
    }
}
