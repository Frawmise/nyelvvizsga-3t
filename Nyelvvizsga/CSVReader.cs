﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nyelvvizsga
{
    public static class CSVReader
    {
        public static Dictionary<string, Dictionary<int, int>> ReadCSV(string filePath)
        {
            var result = new Dictionary<string, Dictionary<int, int>>();

            try
            {
                using (var f = new StreamReader(filePath, Encoding.Default))
                {
                    var headLine = f.ReadLine().Split(';');

                    while (!f.EndOfStream)
                    {
                        var row = f.ReadLine().Split(';');
                        var temporaryDict = new Dictionary<int, int>();

                        for (int i = 0; i < headLine.Length; i++)
                        {
                            if (headLine[i].ToLower() == "nyelv")
                            {
                                result.Add(row[i], temporaryDict);
                            }
                            else
                            {
                                temporaryDict.Add(int.Parse(headLine[i]), int.Parse(row[i]));
                            }
                        }

                        result[row[0]] = temporaryDict;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Hiba történt a feldolgozás közben.", ex);
            }
            return result;
        }
    }
}
